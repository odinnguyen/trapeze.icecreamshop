// <copyright file="IceCreamStoreTests.cs" company="Trapeze Ice Cream">
// Copyright (c) Trapeze Ice Cream. All rights reserved.
// </copyright>

namespace Trapeze.IceCreamShop.Api.Tests
{
    using System;
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Trapeze.IceCreamShop.Api.Controllers;
    using Trapeze.IceCreamShop.Api.ViewModels.IceCreamStore;
    using Trapeze.IceCreamShop.Enums;

    /// <summary>
    /// Test cases for the ice cream shop.
    /// </summary>
    [TestClass]
    public class IceCreamStoreTests
    {
        /// <summary>
        /// Unit test Purchase Ice-cream with valid requests.
        /// </summary>
        [TestMethod]
        public void MakeValidRequests()
        {
            using (var controller = new MockIceCreamStoreController())
            {
                var receipt = controller.CreatePurchase(
                    "SKY WALKER",
                    new PurchaseRequest
                    {
                        AmountGiven = 10,
                        Base = IceCreamBase.CakeCone,
                        Scoops = new List<IceCreamFlavour> { IceCreamFlavour.CookiesAndCream, IceCreamFlavour.MooseTracks, IceCreamFlavour.Vanilla }
                    });
                Assert.AreEqual(3.5M, receipt.Change);

                receipt = controller.CreatePurchase(
                    "SKY WALKER",
                    new PurchaseRequest
                    {
                        AmountGiven = 10,
                        Base = IceCreamBase.Cup,
                        Scoops = new List<IceCreamFlavour> { IceCreamFlavour.CookiesAndCream, IceCreamFlavour.MooseTracks, IceCreamFlavour.Chocolate, IceCreamFlavour.Chocolate }
                    });
                Assert.AreEqual(3.2M, receipt.Change);

                receipt = controller.CreatePurchase(
                    "SKY WALKER",
                    new PurchaseRequest
                    {
                        AmountGiven = 10,
                        Base = IceCreamBase.SugarCone,
                        Scoops = new List<IceCreamFlavour> { IceCreamFlavour.CookiesAndCream, IceCreamFlavour.Vanilla, IceCreamFlavour.MintChocolateChip }
                    });
                Assert.AreEqual(3.5M, receipt.Change);

                receipt = controller.CreatePurchase(
                    "SKY WALKER",
                    new PurchaseRequest
                    {
                        AmountGiven = 10,
                        Base = IceCreamBase.WaffleCone,
                        Scoops = new List<IceCreamFlavour> { IceCreamFlavour.MooseTracks, IceCreamFlavour.Vanilla }
                    });
                Assert.AreEqual(3M, receipt.Change);
            }
        }

        /// <summary>
        /// Unit test Purchase Ice-cream with invalid requests.
        /// </summary>
        [TestMethod]
        public void MakeInvalidRequests()
        {
            using (var controller = new MockIceCreamStoreController())
            {
                TestEmptyParameters(controller);
                TestInvalidMoneyAmount(controller);
                TestNoIceCreamPurchased(controller);
                TestExceedMaximumScoopsAllowed(controller);
                TestSpecialRules(controller);
            }
        }

        /// <summary>
        /// Test empty paramerters.
        /// </summary>
        /// <param name="controller"><see cref="IceCreamStoreController"/>.</param>
        private static void TestEmptyParameters(IceCreamStoreController controller)
        {
            Assert.ThrowsException<ArgumentNullException>(
                () => controller.CreatePurchase(
                    " ", new PurchaseRequest
                    {
                        AmountGiven = 10,
                        Base = IceCreamBase.CakeCone,
                        Scoops = new List<IceCreamFlavour> { IceCreamFlavour.CookiesAndCream, IceCreamFlavour.MooseTracks, IceCreamFlavour.Vanilla }
                    }));

            Assert.ThrowsException<ArgumentNullException>(
               () => controller.CreatePurchase(
                   "SKY WALKER", null));
        }

        /// <summary>
        /// Test special rules.
        /// </summary>
        /// <param name="controller"><see cref="IceCreamStoreController"/>.</param>
        private static void TestSpecialRules(IceCreamStoreController controller)
        {
            // We will not give Cookie Dough flavour in the Sugar Cone base.
            Assert.ThrowsException<Exception>(
                () => controller.CreatePurchase(
                    "SKY WALKER",
                    new PurchaseRequest
                    {
                        AmountGiven = 10,
                        Base = IceCreamBase.SugarCone,
                        Scoops = new List<IceCreamFlavour> { IceCreamFlavour.CookieDough }
                    }),
                Resources.NotAllowed_Combinations);

            // We will not give Strawberry and Mint Chocolate Chip flavours together.
            Assert.ThrowsException<Exception>(
                () => controller.CreatePurchase(
                    "SKY WALKER",
                    new PurchaseRequest
                    {
                        AmountGiven = 10,
                        Base = IceCreamBase.WaffleCone,
                        Scoops = new List<IceCreamFlavour> { IceCreamFlavour.Strawberry, IceCreamFlavour.MintChocolateChip }
                    }),
                Resources.NotAllowed_Combinations);

            // We will not give Cookies And Cream, Moose Tracks, and Vanilla together.
            Assert.ThrowsException<Exception>(
                () => controller.CreatePurchase(
                    "SKY WALKER",
                    new PurchaseRequest
                    {
                        AmountGiven = 10,
                        Base = IceCreamBase.Cup,
                        Scoops = new List<IceCreamFlavour> { IceCreamFlavour.CookiesAndCream, IceCreamFlavour.MooseTracks, IceCreamFlavour.Vanilla, IceCreamFlavour.Chocolate }
                    }),
                Resources.NotAllowed_Combinations);
        }

        /// <summary>
        /// Test invalid money given.
        /// </summary>
        /// <param name="controller"><see cref="IceCreamStoreController"/>.</param>
        private static void TestInvalidMoneyAmount(IceCreamStoreController controller)
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(
                () => controller.CreatePurchase(
                    "SKY WALKER",
                    new PurchaseRequest
                    {
                        AmountGiven = 0,
                        Base = IceCreamBase.WaffleCone,
                        Scoops = new List<IceCreamFlavour> { IceCreamFlavour.Chocolate }
                    }),
                Resources.Amount_Must_GreaterThanZero);

            Assert.ThrowsException<Exception>(
                () => controller.CreatePurchase(
                    "SKY WALKER",
                    new PurchaseRequest
                    {
                        AmountGiven = 3.20M,
                        Base = IceCreamBase.WaffleCone,
                        Scoops = new List<IceCreamFlavour> { IceCreamFlavour.Chocolate }
                    }),
                Resources.NotEnoughMoney);
        }

        /// <summary>
        /// Test no ice-cream purchased.
        /// </summary>
        /// <param name="controller"><see cref="IceCreamStoreController"/>.</param>
        private static void TestNoIceCreamPurchased(IceCreamStoreController controller)
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(
                () => controller.CreatePurchase(
                    "SKY WALKER",
                    new PurchaseRequest
                    {
                        AmountGiven = 1,
                        Base = IceCreamBase.WaffleCone
                    }),
                Resources.Scoops_Must_GreaterThanZero);
        }

        /// <summary>
        /// Test maximum scoops allowed.
        /// </summary>
        /// <param name="controller"><see cref="IceCreamStoreController"/>.</param>
        private static void TestExceedMaximumScoopsAllowed(IceCreamStoreController controller)
        {
            Assert.ThrowsException<Exception>(
                () => controller.CreatePurchase(
                    "SKY WALKER",
                    new PurchaseRequest
                    {
                        AmountGiven = 1,
                        Base = IceCreamBase.CakeCone,
                        Scoops = new List<IceCreamFlavour> { IceCreamFlavour.Vanilla, IceCreamFlavour.Vanilla, IceCreamFlavour.Vanilla, IceCreamFlavour.Vanilla }
                    }),
                string.Format(Resources.NotAllowed_Scoops_Per_Base, "Cake Cone", 3));

            Assert.ThrowsException<Exception>(
                () => controller.CreatePurchase(
                    "SKY WALKER",
                    new PurchaseRequest
                    {
                        AmountGiven = 1,
                        Base = IceCreamBase.Cup,
                        Scoops = new List<IceCreamFlavour> { IceCreamFlavour.Vanilla, IceCreamFlavour.Vanilla, IceCreamFlavour.Vanilla, IceCreamFlavour.Vanilla, IceCreamFlavour.Vanilla }
                    }),
                string.Format(Resources.NotAllowed_Scoops_Per_Base, "Cup", 4));

            Assert.ThrowsException<Exception>(
                () => controller.CreatePurchase(
                    "SKY WALKER",
                    new PurchaseRequest
                    {
                        AmountGiven = 1,
                        Base = IceCreamBase.SugarCone,
                        Scoops = new List<IceCreamFlavour> { IceCreamFlavour.Vanilla, IceCreamFlavour.Vanilla, IceCreamFlavour.Vanilla, IceCreamFlavour.Vanilla }
                    }),
                string.Format(Resources.NotAllowed_Scoops_Per_Base, "Sugar Cone", 3));

            Assert.ThrowsException<Exception>(
                () => controller.CreatePurchase(
                    "SKY WALKER",
                    new PurchaseRequest
                    {
                        AmountGiven = 1,
                        Base = IceCreamBase.WaffleCone,
                        Scoops = new List<IceCreamFlavour> { IceCreamFlavour.Vanilla, IceCreamFlavour.Vanilla, IceCreamFlavour.Vanilla, IceCreamFlavour.Vanilla }
                    }),
                string.Format(Resources.NotAllowed_Scoops_Per_Base, "Waffle Cone", 3));
        }
    }
}
