﻿// <copyright file="MockIceCreamStoreController.cs" company="Trapeze Ice Cream">
// Copyright (c) Trapeze Ice Cream. All rights reserved.
// </copyright>

namespace Trapeze.IceCreamShop.Api.Tests
{
    using Trapeze.IceCreamShop.Api.Controllers;
    using Trapeze.IceCreamShop.Data;

    /// <summary>
    /// Initializes a new instance of the <see cref="MockIceCreamStoreController"/> class.
    /// </summary>
    public class MockIceCreamStoreController : IceCreamStoreController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MockIceCreamStoreController"/> class.
        /// </summary>
        public MockIceCreamStoreController()
            : base(null, null, null)
        {
        }

        /// <summary>
        /// Get IceCreamDbContext.
        /// </summary>
        /// <returns><see cref="IceCreamDbContext"/>.</returns>
        protected override IceCreamDbContext GetIceCreamDbContext()
        {
            return new MockIceCreamDbContext();
        }
    }
}
