﻿// <copyright file="MockIceCreamDbContext.cs" company="Trapeze Ice Cream">
// Copyright (c) Trapeze Ice Cream. All rights reserved.
// </copyright>

namespace Trapeze.IceCreamShop.Api.Tests
{
    using System.Data.Common;
    using Microsoft.Data.Sqlite;
    using Microsoft.EntityFrameworkCore;
    using Trapeze.IceCreamShop.Data;

    /// <summary>
    /// A Mock database context for the ice cream entities.
    /// </summary>
    internal class MockIceCreamDbContext : IceCreamDbContext
    {
        /// <summary>
        /// Underlying DbConnection.
        /// </summary>
        private readonly DbConnection _connection;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockIceCreamDbContext"/> class.
        /// </summary>
        public MockIceCreamDbContext()
            : this(CreateInMemoryDatabase())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MockIceCreamDbContext"/> class.
        /// </summary>
        /// <param name="connection"><see cref="DbConnection"/>.</param>
        public MockIceCreamDbContext(DbConnection connection)
            : base(new DbContextOptionsBuilder<IceCreamDbContext>()
                    .UseSqlite(connection)
                    .Options)
        {
            _connection = connection;
        }

        /// <summary>
        /// Dispose underlying connection.
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();
            _connection.Dispose();
        }

        /// <summary>
        /// Create and open an in memory db.
        /// </summary>
        /// <returns>Openned <see cref="DbConnection"/>.</returns>
        private static DbConnection CreateInMemoryDatabase()
        {
            var connection = new SqliteConnection("DataSource=:memory:");

            // EF Core will use an already open connection when given one, and will never attempt to close it.
            // So the key to using EF Core with an in-memory SQLite database is to open the connection before passing it to EF.
            connection.Open();

            return connection;
        }
    }
}
