﻿// <copyright file="IceCreamDbContext.cs" company="Trapeze Ice Cream">
// Copyright (c) Trapeze Ice Cream. All rights reserved.
// </copyright>

namespace Trapeze.IceCreamShop.Data
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;
    using Trapeze.IceCreamShop.Data.Entities;

    /// <summary>
    /// Creates a database context for the ice cream entities.
    /// </summary>
    public partial class IceCreamDbContext : IdentityDbContext<ApplicationUser>
    {
        /// <summary>
        /// Seed.
        /// </summary>
        /// <param name="builder">Model Builder.</param>
        private static void Seeds(ModelBuilder builder)
        {
            SeedRoles(builder);
            SeedUsers(builder);
            SeedUserRoles(builder);

            SeedIceCreamBases(builder);
            SeedIceCreamFlavours(builder);
            SeedScoopConfigurations(builder);
            SeedRules(builder);
        }

        /// <summary>
        /// Seed <see cref="ApplicationUser"/>.
        /// </summary>
        /// <param name="builder">Model Builder.</param>
        private static void SeedUsers(ModelBuilder builder)
        {
            var ph = new PasswordHasher<ApplicationUser>();

            // Add amosvani
            var appUser = new ApplicationUser
            {
                Id = "d47892bd-a32d-4168-93ef-15681a8d28e2",
                UserName = "amosvani",
                NormalizedUserName = "AMOSVANI",
                Email = "amosvani@TrapezeGroup.ca",
                NormalizedEmail = "AMOSVANI@TRAPEZEGROUP.CA",
                EmailConfirmed = true,
                LockoutEnabled = false
            };
            appUser.PasswordHash = ph.HashPassword(appUser, "TW9zdmFuaXh4");
            builder.Entity<ApplicationUser>().HasData(appUser);

            // Add mcauthon
            appUser = new ApplicationUser
            {
                Id = "6e7b2156-21d6-43fe-a23f-33d37b764214",
                UserName = "mcauthon",
                NormalizedUserName = "MCAUTHON",
                Email = "mcauthon@TrapezeGroup.ca",
                NormalizedEmail = "MCAUTHON@TRAPEZEGROUP.CA",
                EmailConfirmed = true,
                LockoutEnabled = false
            };
            appUser.PasswordHash = ph.HashPassword(appUser, "Q2F1dGhvbnh4");
            builder.Entity<ApplicationUser>().HasData(appUser);

            // Add mdamodred
            appUser = new ApplicationUser
            {
                Id = "3d5b733a-ba1c-4b18-a5db-b2c490d19b38",
                UserName = "mdamodred",
                NormalizedUserName = "MDAMODRED",
                Email = "mdamodred@TrapezeGroup.ca",
                NormalizedEmail = "MDAMODRED@TRAPEZEGROUP.CA",
                EmailConfirmed = true,
                LockoutEnabled = false
            };
            appUser.PasswordHash = ph.HashPassword(appUser, "RGFtb2RyZWR4");
            builder.Entity<ApplicationUser>().HasData(appUser);
        }

        /// <summary>
        /// Seed <see cref="IdentityRole"/>.
        /// </summary>
        /// <param name="builder">Model Builder.</param>
        private static void SeedRoles(ModelBuilder builder)
        {
            builder.Entity<IdentityRole>().HasData(
                new IdentityRole() { Id = Entities.UserRoles.User.Id.ToString(), Name = Entities.UserRoles.User.Name, ConcurrencyStamp = Guid.NewGuid().ToString(), NormalizedName = Entities.UserRoles.User.Name.ToUpperInvariant() },
                new IdentityRole() { Id = Entities.UserRoles.Admin.Id.ToString(), Name = Entities.UserRoles.Admin.Name, ConcurrencyStamp = Guid.NewGuid().ToString(), NormalizedName = Entities.UserRoles.Admin.Name.ToUpperInvariant() });
        }

        /// <summary>
        /// Seed IdentityUserRole.
        /// </summary>
        /// <param name="builder">Model Builder.</param>
        private static void SeedUserRoles(ModelBuilder builder)
        {
            builder.Entity<IdentityUserRole<string>>().HasData(
                new IdentityUserRole<string>() { RoleId = Entities.UserRoles.User.Id.ToString(), UserId = "d47892bd-a32d-4168-93ef-15681a8d28e2" },
                new IdentityUserRole<string>() { RoleId = Entities.UserRoles.User.Id.ToString(), UserId = "6e7b2156-21d6-43fe-a23f-33d37b764214" },
                new IdentityUserRole<string>() { RoleId = Entities.UserRoles.User.Id.ToString(), UserId = "3d5b733a-ba1c-4b18-a5db-b2c490d19b38" });
        }

        /// <summary>
        /// Seed <see cref="Base"/>.
        /// </summary>
        /// <param name="builder">Model Builder.</param>
        private static void SeedIceCreamBases(ModelBuilder builder)
        {
            builder.Entity<Base>().HasData(
                new Base() { Id = Enums.IceCreamBase.CakeCone, Name = "Cake Cone", Price = 3.00M, MaxScoops = 3 },
                new Base() { Id = Enums.IceCreamBase.Cup, Name = "Cup", Price = 3.00M, MaxScoops = 4 },
                new Base() { Id = Enums.IceCreamBase.SugarCone, Name = "Sugar Cone", Price = 3.00M, MaxScoops = 3 },
                new Base() { Id = Enums.IceCreamBase.WaffleCone, Name = "Waffle Cone", Price = 4.00M, MaxScoops = 3 });
        }

        /// <summary>
        /// Seed <see cref="Flavour"/>.
        /// </summary>
        /// <param name="builder">Model Builder.</param>
        private static void SeedIceCreamFlavours(ModelBuilder builder)
        {
            builder.Entity<Flavour>().HasData(
                new Flavour() { Id = Enums.IceCreamFlavour.Chocolate, Name = "Chocolate" },
                new Flavour() { Id = Enums.IceCreamFlavour.CookieDough, Name = "Cookie Dough" },
                new Flavour() { Id = Enums.IceCreamFlavour.CookiesAndCream, Name = "Cookies & Cream" },
                new Flavour() { Id = Enums.IceCreamFlavour.MintChocolateChip, Name = "Mint Chocolate Chip" },
                new Flavour() { Id = Enums.IceCreamFlavour.MooseTracks, Name = "Moose Tracks" },
                new Flavour() { Id = Enums.IceCreamFlavour.Strawberry, Name = "Strawberry" },
                new Flavour() { Id = Enums.IceCreamFlavour.Vanilla, Name = "Vanilla" });
        }

        /// <summary>
        /// Seed <see cref="ScoopConfiguration"/>.
        /// </summary>
        /// <param name="builder">Model Builder.</param>
        private static void SeedScoopConfigurations(ModelBuilder builder)
        {
            builder.Entity<ScoopConfiguration>().HasData(
                new ScoopConfiguration() { Id = 1, Count = 1, Price = 2.00M },
                new ScoopConfiguration() { Id = 2, Count = 2, Price = 3.00M },
                new ScoopConfiguration() { Id = 3, Count = 3, Price = 3.50M },
                new ScoopConfiguration() { Id = 4, Count = 4, Price = 3.80M });
        }

        /// <summary>
        /// Seed <see cref="Rule"/>.
        /// </summary>
        /// <param name="builder">Model Builder.</param>
        private static void SeedRules(ModelBuilder builder)
        {
            var rule1 = new Rule()
            {
                Id = 1,
                IsAllowed = false,
                IceCreamBaseId = Enums.IceCreamBase.SugarCone
            };

            var rule2 = new Rule()
            {
                Id = 2,
                IsAllowed = false
            };

            var rule3 = new Rule()
            {
                Id = 3,
                IsAllowed = false
            };

            var rule4 = new Rule()
            {
                Id = 4,
                IsAllowed = true,
                IceCreamBaseId = Enums.IceCreamBase.CakeCone
            };

            builder.Entity<Rule>().HasData(rule1, rule2, rule3, rule4);

            builder.Entity<Rule>()
                .HasMany(r => r.IceCreamFlavours)
                .WithMany(f => f.Rules)
                .UsingEntity<Dictionary<string, object>>(
                    "RuleFlavour",
                    r => r.HasOne<Flavour>().WithMany().HasForeignKey("FlavourId"),
                    l => l.HasOne<Rule>().WithMany().HasForeignKey("RuleId"),
                    je =>
                    {
                        je.HasKey("RuleId", "FlavourId");
                        je.HasData(
                            new { RuleId = 1, FlavourId = Enums.IceCreamFlavour.CookieDough },
                            new { RuleId = 2, FlavourId = Enums.IceCreamFlavour.Strawberry },
                            new { RuleId = 2, FlavourId = Enums.IceCreamFlavour.MintChocolateChip },
                            new { RuleId = 3, FlavourId = Enums.IceCreamFlavour.CookiesAndCream },
                            new { RuleId = 3, FlavourId = Enums.IceCreamFlavour.MooseTracks },
                            new { RuleId = 3, FlavourId = Enums.IceCreamFlavour.Vanilla },
                            new { RuleId = 4, FlavourId = Enums.IceCreamFlavour.CookiesAndCream },
                            new { RuleId = 4, FlavourId = Enums.IceCreamFlavour.MooseTracks },
                            new { RuleId = 4, FlavourId = Enums.IceCreamFlavour.Vanilla });
                    });
        }
    }
}
