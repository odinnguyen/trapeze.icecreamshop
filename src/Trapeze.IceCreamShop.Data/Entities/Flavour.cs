﻿// <copyright file="Flavour.cs" company="Trapeze Ice Cream">
// Copyright (c) Trapeze Ice Cream. All rights reserved.
// </copyright>

namespace Trapeze.IceCreamShop.Data.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Trapeze.IceCreamShop.Enums;

    /// <summary>
    /// The ice cream Flavour configuration.
    /// </summary>
    public class Flavour
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [Key]
        public IceCreamFlavour Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets The Flavours Informations.
        /// </summary>
        public virtual ICollection<FlavourInformation> FlavourInformations { get; } = new HashSet<FlavourInformation>();

        /// <summary>
        /// Gets The Rules.
        /// </summary>
        public virtual ICollection<Rule> Rules { get; } = new HashSet<Rule>();
    }
}
