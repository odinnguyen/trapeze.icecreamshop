﻿// <copyright file="Rule.cs" company="Trapeze Ice Cream">
// Copyright (c) Trapeze Ice Cream. All rights reserved.
// </copyright>

namespace Trapeze.IceCreamShop.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Trapeze.IceCreamShop.Enums;

    /// <summary>
    /// The special rule.
    /// </summary>
    public class Rule
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this is allowed rule.
        /// </summary>
        public bool IsAllowed { get; set; }

        /// <summary>
        /// Gets or sets the flavour of the ice cream.
        /// </summary>
        public IceCreamBase? IceCreamBaseId { get; set; }

        /// <summary>
        /// Gets or sets the flavour of the ice cream.
        /// </summary>
        public Base IceCreamBase { get; set; }

        /// <summary>
        /// Gets the flavour of the ice cream.
        /// </summary>
        public virtual ICollection<Flavour> IceCreamFlavours { get; } = new HashSet<Flavour>();
    }
}
