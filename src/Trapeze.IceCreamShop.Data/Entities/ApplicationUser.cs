﻿// <copyright file="ApplicationUser.cs" company="Trapeze Ice Cream">
// Copyright (c) Trapeze Ice Cream. All rights reserved.
// </copyright>

namespace Trapeze.IceCreamShop.Data.Entities
{
    using Microsoft.AspNetCore.Identity;

    /// <summary>
    /// Identity User.
    /// </summary>
    public class ApplicationUser : IdentityUser
    {
    }
}
