﻿// <copyright file="Base.cs" company="Trapeze Ice Cream">
// Copyright (c) Trapeze Ice Cream. All rights reserved.
// </copyright>

namespace Trapeze.IceCreamShop.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Trapeze.IceCreamShop.Enums;

    /// <summary>
    /// The ice cream base configuration.
    /// </summary>
    public class Base
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [Key]
        public IceCreamBase Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the maximum number of scoops allowed.
        /// </summary>
        public int MaxScoops { get; set; }

        /// <summary>
        /// Gets the Base Informations.
        /// </summary>
        public virtual ICollection<BaseInformation> BaseInformations { get; } = new HashSet<BaseInformation>();
    }
}
