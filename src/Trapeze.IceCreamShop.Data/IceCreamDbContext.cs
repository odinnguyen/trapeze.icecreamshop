﻿// <copyright file="IceCreamDbContext.cs" company="Trapeze Ice Cream">
// Copyright (c) Trapeze Ice Cream. All rights reserved.
// </copyright>

namespace Trapeze.IceCreamShop.Data
{
    using System;
    using System.IO;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Trapeze.IceCreamShop.Data.Configurations;
    using Trapeze.IceCreamShop.Data.Entities;

    /// <summary>
    /// Creates a database context for the ice cream entities.
    /// </summary>
    public partial class IceCreamDbContext : IdentityDbContext<ApplicationUser>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IceCreamDbContext"/> class.
        /// </summary>
        public IceCreamDbContext()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IceCreamDbContext"/> class.
        /// </summary>
        /// <param name="options">The database context options.</param>
        public IceCreamDbContext(
            DbContextOptions<IceCreamDbContext> options)
            : base(options)
        {
            Database.OpenConnection();
            Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets Master table of ice-cream base.
        /// </summary>
        public DbSet<Base> Bases { get; set; }

        /// <summary>
        /// Gets or sets Master table of ice-cream flavours.
        /// </summary>
        public DbSet<Flavour> Flavours { get; set; }

        /// <summary>
        /// Gets or sets Master table of Scoop Configurations.
        /// </summary>
        public DbSet<ScoopConfiguration> ScoopConfigurations { get; set; }

        /// <summary>
        /// Gets or sets the purchase requests.
        /// </summary>
        public DbSet<IceCreamInformation> IceCreamPurchases { get; set; }

        /// <summary>
        /// Gets or sets the purchase requests's base.
        /// </summary>
        public DbSet<BaseInformation> BaseInformations { get; set; }

        /// <summary>
        /// Gets or sets the purchase requests's scoop(s).
        /// </summary>
        public DbSet<FlavourInformation> FlavourInformations { get; set; }

        /// <summary>
        /// Gets or sets the special rules.
        /// </summary>
        public DbSet<Rule> Rules { get; set; }

        /// <summary>
        /// Override OnConfiguring to initialize underlying connection.
        /// </summary>
        /// <param name="optionsBuilder">DbContextOptionsBuilder.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder == null)
            {
                return;
            }

            base.OnConfiguring(optionsBuilder);
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json")
                   .Build();
                var connectionString = configuration.GetConnectionString("IceCreamDb");
                optionsBuilder.UseSqlite(connectionString);
            }
        }

        /// <summary>
        /// Defines the model that entity framework will retrieve.
        /// </summary>
        /// <param name="builder">The builder to create the model with.</param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            builder.ApplyConfiguration(new IceCreamInformationConfig());
            builder.ApplyConfiguration(new BaseInformationConfig());
            builder.ApplyConfiguration(new FlavourInformationConfig());
            base.OnModelCreating(builder);

            Seeds(builder);
        }
    }
}
