﻿// <copyright file="IceCreamStoreController.cs" company="Trapeze Ice Cream">
// Copyright (c) Trapeze Ice Cream. All rights reserved.
// </copyright>

namespace Trapeze.IceCreamShop.Api.Controllers
{
    using System;
    using System.Dynamic;
    using System.Linq;
    using System.Net.Mime;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Trapeze.IceCreamShop.Api.ViewModels.IceCreamStore;
    using Trapeze.IceCreamShop.Data;
    using Trapeze.IceCreamShop.Data.Entities;

    /// <summary>
    /// An API controller for performing actions on the ice cream store.
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class IceCreamStoreController : MyBaseController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IceCreamStoreController"/> class.
        /// </summary>
        /// <param name="userManager">User Manager.</param>
        /// <param name="roleManager">Role Manager.</param>
        /// <param name="configuration">configuration.</param>
        public IceCreamStoreController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
            : base(userManager, roleManager, configuration)
        {
        }

        /// <summary>
        /// To Make Purchase request.
        /// </summary>
        /// <param name="model">purchase request.</param>
        /// <returns><see cref="IActionResult"/>.</returns>
        [Route("Purchases")]
        [Authorize]
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        public IActionResult MakePurchase(PurchaseRequest model)
        {
            try
            {
                return Ok(CreatePurchase(User.Identity.Name.ToUpperInvariant(), model));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
        }

        /// <summary>
        /// To Make Purchase request.
        /// </summary>
        /// <param name="username">requester's name.</param>
        /// <param name="model">purchase request.</param>
        /// <returns><see cref="PurchaseReceipt"/>.</returns>
        [ApiExplorerSettings(IgnoreApi = true)]
        public PurchaseReceipt CreatePurchase(string username, PurchaseRequest model)
        {
            if (string.IsNullOrWhiteSpace(username))
            {
                throw new ArgumentNullException(nameof(username));
            }

            if (model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }

            if (model.AmountGiven <= 0)
            {
                throw new ArgumentOutOfRangeException(Resources.Amount_Must_GreaterThanZero);
            }

            if (model.Scoops == null || model.Scoops.Count == 0)
            {
                throw new ArgumentOutOfRangeException(Resources.Scoops_Must_GreaterThanZero);
            }

            using (var context = GetIceCreamDbContext())
            {
                var iceCreamBase = context.Bases.FirstOrDefault(b => b.Id == model.Base);
                if (iceCreamBase == null)
                {
                    throw new ArgumentException(string.Format(Resources.NotExists_Base, model.Base.ToString()));
                }

                if (iceCreamBase.MaxScoops < model.Scoops.Count)
                {
                    throw new Exception(string.Format(Resources.NotAllowed_Scoops_Per_Base, iceCreamBase.Name, iceCreamBase.MaxScoops));
                }

                // Check special rule which allows this combination
                var rulesAllow = context.Rules.Where(r => r.IsAllowed
                        && (r.IceCreamBaseId == null || r.IceCreamBaseId == model.Base)
                        && r.IceCreamFlavours.Count == model.Scoops.Count)
                    .Include(r => r.IceCreamFlavours);
                var valid = false;
                foreach (var ruleAllow in rulesAllow)
                {
                    var groups = ruleAllow.IceCreamFlavours.GroupBy(f => f.Id).Select(g => new { IceCreamFlavour = g.Key, Count = g.Count() });
                    if (groups.All(g => g.Count == model.Scoops.Where(s => s == g.IceCreamFlavour).Count()))
                    {
                        valid = true;
                        break;
                    }
                }

                // Check rule which doesn't allow this combination
                var rulesNotAllow = context.Rules.Where(r => !r.IsAllowed && (r.IceCreamBaseId == null || r.IceCreamBaseId == model.Base))
                    .Include(r => r.IceCreamFlavours);
                var invalid = false;
                foreach (var ruleNotAllow in rulesNotAllow)
                {
                    var groups = ruleNotAllow.IceCreamFlavours.GroupBy(f => f.Id).Select(g => new { IceCreamFlavour = g.Key, Count = g.Count() });
                    if (groups.All(g => g.Count <= model.Scoops.Where(s => s == g.IceCreamFlavour).Count()))
                    {
                        invalid = true;
                        break;
                    }
                }

                if (!valid && invalid)
                {
                    throw new Exception(Resources.NotAllowed_Combinations);
                }

                var iceCreamCost = context.ScoopConfigurations.Where(sc => sc.Count == model.Scoops.Count).Select(sc => sc.Price).FirstOrDefault();
                var subTotal = iceCreamBase.Price + iceCreamCost;

                if (model.AmountGiven < subTotal)
                {
                    throw new Exception(Resources.NotEnoughMoney);
                }

                var purchase = new IceCreamInformation
                {
                    PurchaseAmount = subTotal,
                    PurchaserName = username,
                    PurchaseDateTime = DateTime.UtcNow,
                    Base = new BaseInformation { IceCreamBaseId = model.Base }
                };

                foreach (var flavour in model.Scoops)
                {
                    purchase.Flavours.Add(new FlavourInformation { IceCreamFlavourId = flavour });
                }

                context.IceCreamPurchases.Add(purchase);
                context.SaveChanges();

                return new PurchaseReceipt
                {
                    PurchaseId = purchase.Id,
                    Base = model.Base,
                    BasePrice = iceCreamBase.Price,
                    Scoops = model.Scoops,
                    IceCreamPrice = iceCreamCost,
                    SubTotal = subTotal,
                    AmountGiven = model.AmountGiven,
                    Change = model.AmountGiven - subTotal
                };
            }
        }
    }
}
