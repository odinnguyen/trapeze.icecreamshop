﻿// <copyright file="MyBaseController.cs" company="Trapeze Ice Cream">
// Copyright (c) Trapeze Ice Cream. All rights reserved.
// </copyright>

namespace Trapeze.IceCreamShop.Api.Controllers
{
    using System;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Trapeze.IceCreamShop.Data;
    using Trapeze.IceCreamShop.Data.Entities;

    /// <summary>
    /// Common Base Controller.
    /// </summary>
    public abstract class MyBaseController : ControllerBase, IDisposable
    {
        // To detect redundant calls
        private bool _disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="MyBaseController"/> class.
        /// </summary>
        /// <param name="userManager">User Manager.</param>
        /// <param name="roleManager">Role Manager.</param>
        /// <param name="configuration">configuration.</param>
        protected MyBaseController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
        {
            UserManager = userManager;
            RoleManager = roleManager;
            Configuration = configuration;
        }

        /// <summary>
        /// Gets User Manager.
        /// </summary>
        protected UserManager<ApplicationUser> UserManager { get; private set; }

        /// <summary>
        /// Gets Role Manager.
        /// </summary>
        protected RoleManager<IdentityRole> RoleManager { get; private set; }

        /// <summary>
        /// Gets Configuration.
        /// </summary>
        protected IConfiguration Configuration { get; private set; }

        /// <summary>
        /// Dispose.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose.
        /// </summary>
        /// <param name="disposing">disposing.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                if (UserManager != null)
                {
                    UserManager.Dispose();
                    UserManager = null;
                }

                if (RoleManager != null)
                {
                    RoleManager.Dispose();
                    RoleManager = null;
                }
            }

            _disposed = true;
        }

        /// <summary>
        /// Get <see cref="IceCreamDbContext"/>.
        /// </summary>
        /// <returns><see cref="IceCreamDbContext"/>.</returns>
        protected virtual IceCreamDbContext GetIceCreamDbContext()
        {
            return new IceCreamDbContext();
        }
    }
}
