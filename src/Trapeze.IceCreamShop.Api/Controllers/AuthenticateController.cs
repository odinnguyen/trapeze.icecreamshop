﻿// <copyright file="AuthenticateController.cs" company="Trapeze Ice Cream">
// Copyright (c) Trapeze Ice Cream. All rights reserved.
// </copyright>

namespace Trapeze.IceCreamShop.Api.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IdentityModel.Tokens.Jwt;
    using System.Net.Mime;
    using System.Security.Claims;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.IdentityModel.Tokens;
    using Trapeze.IceCreamShop.Api.ViewModels.Authentication;
    using Trapeze.IceCreamShop.Data.Entities;

    /// <summary>
    /// Authentication controller.
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    public class AuthenticateController : MyBaseController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticateController"/> class.
        /// </summary>
        /// <param name="userManager">User Manager.</param>
        /// <param name="roleManager">Role Manager.</param>
        /// <param name="configuration">configuration.</param>
        public AuthenticateController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
            : base(userManager, roleManager, configuration)
        {
        }

        /// <summary>
        /// Get Login Token.
        /// </summary>
        /// <param name="model">Login Model.</param>
        /// <returns>Login Token.</returns>
        [HttpPost]
        [Route("login")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            var user = await UserManager.FindByNameAsync(model?.Username).ConfigureAwait(false);
            if (user != null && await UserManager.CheckPasswordAsync(user, model.Password).ConfigureAwait(false))
            {
                var userRoles = await UserManager.GetRolesAsync(user).ConfigureAwait(false);

                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };

                foreach (var userRole in userRoles)
                {
                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                }

                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:Secret"]));

                var token = new JwtSecurityToken(
                    issuer: Configuration["JWT:ValidIssuer"],
                    audience: Configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddHours(24),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256));

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo
                });
            }

            return Unauthorized();
        }

        /// <summary>
        /// Register new user.
        /// </summary>
        /// <param name="model">Register Model.</param>
        /// <returns>Status.</returns>
        [HttpPost]
        [Route("register")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            var userExists = await UserManager.FindByNameAsync(model?.Username).ConfigureAwait(false);
            if (userExists != null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = Resources.UserExisted });
            }

            ApplicationUser user = new ApplicationUser()
            {
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.Username
            };
            var result = await UserManager.CreateAsync(user, model.Password).ConfigureAwait(false);
            if (!result.Succeeded)
            {
                var errorMsg = "User creation failed! Please check user details and try again.";
                foreach (var error in result.Errors)
                {
                    errorMsg += Environment.NewLine + error.Description;
                }

                return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = errorMsg });
            }

            if (!await RoleManager.RoleExistsAsync(UserRoles.User.Name).ConfigureAwait(false))
            {
                await RoleManager.CreateAsync(new IdentityRole(UserRoles.User.Name)).ConfigureAwait(false);
            }

            await UserManager.AddToRoleAsync(user, UserRoles.User.Name).ConfigureAwait(false);

            return Ok(new Response { Status = "Success", Message = "User created successfully!" });
        }
    }
}
