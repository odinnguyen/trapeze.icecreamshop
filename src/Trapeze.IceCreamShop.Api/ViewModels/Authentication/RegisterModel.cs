﻿// <copyright file="RegisterModel.cs" company="Trapeze Ice Cream">
// Copyright (c) Trapeze Ice Cream. All rights reserved.
// </copyright>

namespace Trapeze.IceCreamShop.Api.ViewModels.Authentication
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// User registration model.
    /// </summary>
    public class RegisterModel
    {
        /// <summary>
        /// Gets or sets Username.
        /// </summary>
        [Required(ErrorMessage = "User Name is required")]
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets User's Email.
        /// </summary>
        [EmailAddress]
        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets Password.
        /// </summary>
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
    }
}
