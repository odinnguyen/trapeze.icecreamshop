﻿// <copyright file="Response.cs" company="Trapeze Ice Cream">
// Copyright (c) Trapeze Ice Cream. All rights reserved.
// </copyright>

namespace Trapeze.IceCreamShop.Api.ViewModels.Authentication
{
    /// <summary>
    /// Response Model.
    /// </summary>
    public class Response
    {
        /// <summary>
        /// Gets or sets Status Name.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets Message.
        /// </summary>
        public string Message { get; set; }
    }
}
