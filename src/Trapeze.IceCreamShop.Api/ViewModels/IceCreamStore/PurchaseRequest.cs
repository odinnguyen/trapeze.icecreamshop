﻿// <copyright file="PurchaseRequest.cs" company="Trapeze Ice Cream">
// Copyright (c) Trapeze Ice Cream. All rights reserved.
// </copyright>

namespace Trapeze.IceCreamShop.Api.ViewModels.IceCreamStore
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Trapeze.IceCreamShop.Enums;

    /// <summary>
    /// Purchase Request View Model.
    /// </summary>
    public class PurchaseRequest
    {
        /// <summary>
        /// Gets or sets total Amount given.
        /// </summary>
        [Required]
        public decimal AmountGiven { get; set; }

        /// <summary>
        /// Gets or sets base.
        /// </summary>
        [Required]
        public IceCreamBase Base { get; set; }

        /// <summary>
        /// Gets or sets base.
        /// </summary>
        [Required]
        public ICollection<IceCreamFlavour> Scoops { get; set; }
    }
}
