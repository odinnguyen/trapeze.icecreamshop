﻿// <copyright file="PurchaseReceipt.cs" company="Trapeze Ice Cream">
// Copyright (c) Trapeze Ice Cream. All rights reserved.
// </copyright>

namespace Trapeze.IceCreamShop.Api.ViewModels.IceCreamStore
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Purchase Receipt View Model.
    /// </summary>
    public class PurchaseReceipt : PurchaseRequest
    {
        /// <summary>
        /// Gets or sets purchase's id.
        /// </summary>
        [Required]
        public int PurchaseId { get; set; }

        /// <summary>
        /// Gets or sets the price of ice cream base.
        /// </summary>
        public decimal BasePrice { get; set; }

        /// <summary>
        /// Gets or sets the price of ice creams.
        /// </summary>
        public decimal IceCreamPrice { get; set; }

        /// <summary>
        /// Gets or sets the subtotal.
        /// </summary>
        public decimal SubTotal { get; set; }

        /// <summary>
        /// Gets or sets the change amount.
        /// </summary>
        public decimal Change { get; set; }
    }
}
